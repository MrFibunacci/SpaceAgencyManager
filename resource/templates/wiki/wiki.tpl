{extends file='main.tpl'}

{$page_name = "Wiki"}

{$breadcrumbPath = [
    ['path' => '', 'text' => $page_name]
]}

{block name='title'} - {$page_name}{/block}
{block name='page_header'}{$page_name}{/block}
{block name='page_content'}
    <div class="row">
        <div class="col-12">
            <div class="jumbotron">
                <h1 class="text-center">Welcome to the official Space Flight Manager wiki site</h1>
                <p>Here you can see the content easily and clearly. They are separated in topics. You can read what the topic is at the top of the list.</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    First Steps
                </div>
                <div class="card-body">
                    <ul class="list-inline">
                        <li><a href="/wiki/beginners_guide">Beginners guide</a></li>
                        <li><a href="/wiki/agency">Agency</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    Missions
                </div>
                <div class="card-body">
                    <ul class="list-inline">
                        <li><a href="">Mission</a></li>
                        <li><a href="">Planing</a></li>
                        <li><a href="">Assemble craft</a></li>
                        <li><a href="">Moving hardware</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    Finances
                </div>
                <div class="card-body">
                    <ul class="list-inline">
                        <li><a href="">General income</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    Agency
                </div>
                <div class="card-body">
                    <ul class="list-inline">
                        <li><a href="">Locations</a></li>
                        <li><a href="">Employees</a></li>
                        <li><a href="">Reputation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
{/block}

{block name='extra_scripts'}{/block}
{extends file='main.tpl'}

{$page_name = "Agency"}

{$breadcrumbPath = [
    ['path' => '/wiki', 'text' => 'Wiki'],
    ['path' => '', 'text' => $page_name]
]}

{block name='title'} - {$page_name}{/block}
{block name='page_header'}{$page_name}{/block}
{block name='page_content'}
    <div class="row">
        <div class="col">
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Version<span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="/wiki/agency/1">v1</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
            <h1 id="introduction">Introduction</h1>
            <p>TODO: Here shall go what the heck an agency is!</p>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    Table of content
                </div>
                <div class="card-body">
                    <ol>
                        <li><a href="#introduction">Introduction</a></li>
                        <li><a href="#budget">Budget</a></li>
                        <li><a href="#employees">Employees</a></li>
                        <li><a href="#location">Locations</a></li>
                        <li><a href="#transport">Transport</a></li>
                        <li><a href="#reputation">Reputation</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <h1 id="budget">Budget</h1>
    <p>Due to government support the Space Agency gets every "in game" Year an amount of money which depends on the reputation.</p>

    <hr>
    <h1 id="employees">Employees</h1>
    <p>An employee has an subject area. How good they are in that area defined the quality of there work as well as the amount of money they want.</p>
    <p>Those subject areas are:</p>
    <ul>
        <li>Engineer</li>
        <li>Manager</li>
        <li>Constructor</li>
        <li>Scientist</li>
        <li>Software developer</li>
    </ul>

    <hr>
    <h1 id="location">Locations</h1>
    <p>You require some Buildings where you manage your missions, build stuff and store builded stuff as well as launch your rockets. You can buy existing ones or build new.</p>

    <h2 id="transport">Transport</h2>
    <p>If the place where you assemble the rocket if far apart from where it shall be launched is an transportation required. There are multiple ways to do so.</p>

    <hr>
    <h1 id="reputation">Reputation</h1>
    <p>Increases if you finsh missions successfully, decreases if you fail.</p>
{/block}

{block name='extra_scripts'}{/block}
{extends file='main.tpl'}

{$page_name = "Beginners guide"}

{$breadcrumbPath = [
    ['path' => '/wiki', 'text' => 'Wiki'],
    ['path' => '', 'text' => $page_name]
]}

{block name='title'} - {$page_name}{/block}
{block name='page_header'}{$page_name}{/block}
{block name='page_content'}
    <div class="row">
        <div class="col">
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Version<span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="/wiki/beginners_guide/1">v1</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
            <h1 id="introduction">Introduction</h1>
            <p><b>Space Agency Manager</b> is an webbrowser based game where you manage an commercial space <a href="/wiki/agency">agency</a> in an "fictive" world mostly similar to the current except that your agency exists alongside with the beginning of the "space age".</p>
            <p>The beginning of the "space age" is generally equated with the launch of the first Sputnik satellite on October 4, 1957. However, the term "space age" was already used shortly after the end of the second world war in Harry Harper's book "Dawn of the Space Age" and referred to the height record set by the German V2 missiles in 1944. We will start our journey in 1957. Were you develop your technology alongside with the great ones.</p>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    Table of content
                </div>
                <div class="card-body">
                    <ol>
                        <li><a href="#introduction">Introduction</a></li>
                        <li><a href="#game_overview">Game Overview</a></li>
                        <li><a href="#first_steps">First steps</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <h1 id="game_overview">Game Overview</h1>
    <p>You simply Build up your agency, as if you would start one in real life, research technology and explore our Solar System. Its going to be an semi self playing game, because it takes "real time" time to do research or to get your craft somewhere or simply to build it. </p>

    <hr>
    <h1 id="first_steps">First steps</h1>
    <p>First of all it is necessary that you have an account created and you need to be logged in.</p>
{/block}

{block name='extra_scripts'}{/block}
{extends file='main.tpl'}

{$page_name = "Employees"}

{$breadcrumbPath = [
    ['link' => '', 'text' => $page_name]
]}

{block name='title'} - {$page_name}{/block}
{block name='page_header'}{$page_name}{/block}
{block name='page_content'}
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    Your employees
                </div>
                <div class="body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Subject Area</th>
                                <th>Currently working on</th>
                                <th>Hired Since</th>
                                <th>Experience</th>
                                <th>Annual salary</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>1</td>
                                <td>Misty Meerkat</td>
                                <td>Engineer</td>
                                <td><b>Test</b> in <b>VAB</b></td>
                                <td>1976-06-13</td>
                                <td>16 years</td>
                                <td>45.000$</td>
                                <td>
                                    <div class="dropdown show">
                                        <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                            <i class="fa fa-fw fa-ellipsis-h"></i>
                                        </button>
                                        <div class="dropdown-menu" style="position: absolute; transform: translate3d(0px, -52px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <a class="dropdown-item" href=""></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    Your applicants
                </div>
                <div class="body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Subject Area</th>
                                <th>Experience</th>
                                <th>Annual salary</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

{/block}

{block name='extra_scripts'}{/block}
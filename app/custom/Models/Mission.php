<?php
    namespace app\custom\Models;

    use app\framework\Component\http\Model\Model;
    use app\framework\Component\StdLib\SingletonTrait;

    class Mission extends Model
    {
        use SingletonTrait;

        protected $table = 'missions';

        protected $connection = 'mysql';

        public function getMissions($agencyID)
        {
            return $this->select('*', ['agency' => $agencyID]);
        }

        public function getMission($agencyID, $missionID)
        {
            return $this->select('*', ['id' => $missionID, 'agency' => $agencyID]);
        }

        public function addMission($name, $agencyID)
        {
            return $this->insert([
                'name' => $name,
                'agency' => $agencyID,

                // TODO: think of some default status for new missions. One maybe?
                //'status' =>
            ]);
        }

        public function getHardwareList($missionID)
        {
            // TODO: consider 'exporting' this to satellite model.
            // TODO: do same with every other hardware available.
            $result = $this->select('*', ['assignedMission' => $missionID], 'satellite');
            $satelliteList = [];

            foreach ($result as $item){
                $item['type'] = 'Satellite';
                $satelliteList[] = $item;
            }

            return $satelliteList;
        }

        public function getLogList($missionID)
        {
            $log = $this->select('*', ['mission_id' => $missionID], 'missionLog');
            $tempLog = $log;

            foreach ($tempLog as $key => $item)
                $log[$key]['label'] = $this->select('label', ['id' => $item['label']], 'missionLogLabels')[0];

            return $log;
        }

        public function addMissionLog($missionID, $logLabelID)
        {
            // we do this because log labels will be predefined.
            if($this->count(['id' => $logLabelID], 'missionLogLabel') > 0)
                $this->insert(['dateTime' => 'NOW()', 'mission_id' => $missionID, 'label' => $logLabelID], 'missionLog');
        }
    }
<?php
    
    namespace app\custom\Http\Controller;
    
    use app\framework\Component\StdLib\SingletonTrait;

    class Wiki
    {
        use SingletonTrait;

        static $path = 'wiki/';

        public function get($name, $version)
        {
            switch ($name) {
                case 'wiki': $this->wiki($version); break;
                case 'beginners_guide': $this->beginners_guide($version); break;
            }
        }

        public function wiki($version)
        {
            switch ($version) {
                case '1':
                default:
                    view(self::$path.'wiki');
                    break;
            }
        }

        public function beginners_guide($version)
        {
            switch ($version) {
                case '1':
                default:
                    view(self::$path.'beginners_guide');
                    break;
            }
        }
    }
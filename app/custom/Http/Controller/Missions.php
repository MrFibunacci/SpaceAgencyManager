<?php
    
    namespace app\custom\Http\Controller;
    
    use app\custom\Models\Mission;

    class Missions
    {
        public static function overview(int $agencyID)
        {
            $missions = Mission::getInstance()->getMissions($agencyID);

            view('missions', [
                'agencyID' => $agencyID,
                'listEntrys' => $missions
            ]);
        }

        public static function showOne($agencyID, $missionID)
        {
            $missionData  = Mission::getInstance()->getMission($agencyID, $missionID)[0];
            $hardwareList = Mission::getInstance()->getHardwareList($missionID);
            $logList      = Mission::getInstance()->getLogList($missionID);
            $canBeAdded   = [];

            view('mission', [
                'agencyID' => $agencyID,
                'missionData' => $missionData,
                'hardwareList' => $hardwareList,
                'logList' => $logList,
                'canBeAdded' => $canBeAdded
            ]);
        }
    }